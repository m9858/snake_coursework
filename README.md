# Snake-Сoursework


## About the game

The player controls a long, thin creature resembling a snake, which crawls along a plane (usually bounded by walls), collecting food (or other objects), avoiding collisions with its own tail and the edges of the playing field (there are options where, when passing through the edge, the snake comes out of the opposite edge of the field). Each time the snake eats a piece of food, it becomes longer, which gradually complicates the game.
In another variant, two people play with two such snakes in such a way as to force the opponent to crash into something.

## Collecting the game

You can build an application through qt creator, but an error may occur that there will be no qt multimedia and it will have to be updated, or you can do it more correctly and in a simple way through qmake:

``` 
    $ qmake 
    $ make -j5
    $ bin/Snake
```


## Control

Control of the game takes place using:

| Keys              | Actions                    |
| ----------------- | -------------------------- |
| Key\_Left         | Turn left                  |
| Key\_Right        | Turn right                 |
| Key\_Up           | Turn up                    |
| Key\_Down         | Turn down                  |
| ESC               | Pauses/Unpauses the game   |
